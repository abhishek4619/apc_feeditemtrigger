/***************APC_ChatterTriggerHandler****************
@Author Abhishek
@Date 26/02/2019
@Description Controller class to Trigger wh is created with Account which consist AccountTeamMember
will replicate same in the OpportunityTeamMember
**********************************************/

public class APC_ChatterTriggerHandler {
    // @Description : Engagement_Scoring__mdt is a custom metadata type where action=chatter post
    //  			   checking if custom metadata has  value and Monthly_Community_Engagement__c custom object has value 
    //  			   Community_Engagement__c is updated else its is created
    //  
    // @Method : createEngagementScoring
    // 
    // @Param : feedItems
    // 
    // @Return : null
    public static void createEngagementScoring(List<FeedItem> feedItems){
        system.debug('feedItems--->'+feedItems);
        List<Engagement_Scoring__mdt> engagementScoringList = [SELECT Id, Action__c  FROM Engagement_Scoring__mdt WHERE Action__c='Chatter Post'];
        List<Monthly_Community_Engagement__c > monthlyCommtEngageList = [select id,Start_Date__c,Month__c,Owner__c,Total_Points__c from Monthly_Community_Engagement__c];
        List<Community_Engagement__c> communityEngagementList=[SELECT id,Action__c,Link__c,Monthly_Community_Engagement__c,Owner__c,Points__c from Community_Engagement__c ];
        System.debug('monthlyCommtEngageList: ' + monthlyCommtEngageList);
        System.debug('engagementScoringList: '+ engagementScoringList);
        try{
            if(engagementScoringList.size() > 0) {
                if(monthlyCommtEngageList.size() > 0) {
                    Community_Engagement__c commtyEngagement=new Community_Engagement__c(Monthly_Community_Engagement__c = monthlyCommtEngageList[0].Id);
                    insert commtyEngagement;          
                    System.debug('IN IF: '+ commtyEngagement);
                } else {
                    Monthly_Community_Engagement__c monthlyCommunityEngage = new Monthly_Community_Engagement__c();
                    insert monthlyCommunityEngage;
                    Community_Engagement__c communityEngagement = new Community_Engagement__c(Monthly_Community_Engagement__c = monthlyCommunityEngage.Id);
                    insert communityEngagement;
                    System.debug('Monthly_Community_Engagement__c: '+ monthlyCommunityEngage);
                    System.debug('Community_Engagement__c: '+ communityEngagement);
                }
                
            } 
        } catch(Exception e){
            system.debug('error'+ e.getMessage());
        } 
    }
    
}